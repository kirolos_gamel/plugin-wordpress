==Developer: Kirolos Ibrahim

Here is a short description of the plugin.  This should be no more than 150 characters.  No markup here.

== Description ==
This Plugin: is registration form for financial market, register information that users entered and their budget, that they want to invest in market. It contain from two parts:

1-Front End: contain from two parts: 

A-Form on front end: contain main three elements: name, email, and budget. And if admin check on comment element, then form will contain comment element with some description admin enter by himself. You can display this form by shortcode on any page.

B-Data: you can display all data that all registered by users.

2-Backend: contain from two parts:

A-Data: display all data that registered by users, and admin can delete or edit them.

B-Setting: admin can config form by add comment element, and add the description that he want to display with comment element. And also setting contain shortcode to display form and data on frontpage.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
