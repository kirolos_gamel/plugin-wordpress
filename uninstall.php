<?php
if(!defined('WP_UNINSTALL_PLUGIN'))
    exit;

    /** following code deletes all tables
     * and the plugin options after uninstall.
     */
global $wpdb;

$table_name = $wpdb->prefix . "register_info";


$wpdb->query("DROP TABLE IF EXISTS ". $table_name);

