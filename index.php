<?php
/*
Plugin Name: Register Information
Plugin URI: http://piqibo.me.pn/data-storage-plugin/
Description: Take information from Users: name-eamil-budget for how much money they want to invest in financial market.
Author: kirolos
Version: 0.1
*/

/*==========================================================
create table for plugin when Activate
=============================================================
*/

 function install() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'register_info';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        name varchar(150) NOT NULL,
        email varchar(150) NOT NULL,
        budget int(6) NOT NULL,
        comment text NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}

   register_activation_hook(__FILE__, 'install' );


add_action('admin_menu','info_admin_menu');

function info_admin_menu(){
    add_menu_page("Register Information","Register Information",4,"register-information","registerInformation");

    add_submenu_page( "register-information", "Data", "Data",4, "register-data", "registerInformation" );

    add_submenu_page( "register-information", "Settings", "Settings" ,4, "setting-register-data", "setting_register_data" );
}

function registerInformation(){
 require_once('backend-pages/data.php');
    //require_once('frontend-pages/form.php');
}

function edit_registerInformation(){
    require_once('backend-pages/edit_data.php');
}

function edit_data_admin_menu(){
    $page_suffix = add_submenu_page(null,'Edit Data','Edit Data','manage_options','edit-data','edit_registerInformation');
    //add_action('admin_print_scripts-' . $page_suffix, 'ghazale_ds_admin_field_ext');
}
add_action('admin_menu','edit_data_admin_menu');


/*
  general settings
 */
function register_settings(){
    register_setting('info_settings','add_comment');
    register_setting('info_settings','comment_msg');
}
add_action('admin_init','register_settings');


function setting_register_data(){
    echo "<h1>Settings:</h1>";
    echo '<h4>Shortcode for Add Form: Shortcode : [info-register-short-code]</h4>';
    echo '<h4>Shortcode for Data Registered: Shortcode : [data-register-short-code]</h4>';
    ?>
    <form action="options.php" id="info_settings" method="post">
        <?php settings_errors(); ?>
        <?php settings_fields('info_settings'); ?>
        <div id="settings">
            <h3>Comment For User</h3>
            <div>
                <h4>Comment</h4>
                <input type="checkbox" name="add_comment" id="add_comment" value="1" <?php
                checked(get_option('add_comment'), 1);
                ?>/> I want to add comment to user in frontend, user can write description for anything you want, like which field he want to invest his money. <i></i> <br><br>
                Add Custom Comment Message: <br>
        <textarea rows="4" cols="40" name="comment_msg"><?php
            if(get_option('comment_msg')){
                echo get_option('comment_msg');
            } else{
                echo "Comment";
            }
            ?></textarea><br><br>
                
            </div>
  

        </div>
        <br>
        <input type="submit" name="save_settings" value="Save Settings">
    </form>
<?php
}


function register_information_style_script(){
    wp_enqueue_script('register_info_js');
    wp_enqueue_style('register_info_style');
}

function register_information_style(){
    wp_enqueue_style('register_info_js',plugins_url('js/bootstrap.min.js',__FILE__));

    wp_enqueue_style('register_info_style',plugins_url('css/bootstrap.min.css',__FILE__));
}
add_action('admin_init','register_information_style');
add_action('wp_enqueue_scripts','register_information_style');





/**
 * showing forms on front end via shortcode with attributes
 */

function frontend_forms_shortcode(){
    require_once('frontend-pages/form.php');
}
add_shortcode('info-register-short-code','frontend_forms_shortcode');

function frontend_data_shortcode(){
    require_once('frontend-pages/data.php');
}
add_shortcode('data-register-short-code','frontend_data_shortcode');

?>